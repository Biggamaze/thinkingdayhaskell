{- 
  Przykład czegoś co może się nie udać:
  Jeżeli w trakcie dodawanie przekroczymy jakąs sumę to zwracamy to jako błąd.
-}

eitherN :: (Num a, Ord a) => a -> a -> Either String a
eitherN next cur 
  | sum < 10 = Right sum
  | otherwise = Left "More than 10!"
  where sum = cur + next

nums :: (Num a, Ord a) => a -> a -> Maybe a
nums next cur 
  | sum < 10  = Just sum
  | otherwise = Nothing
  where sum = cur + next

{- 
  A teraz las ifów :D
-}

exNums1 = case nums 1 0 of
  Nothing   -> Nothing 
  Just sum1 -> case nums 8 sum1 of
    Nothing   -> Nothing
    Just sum2 -> case nums (-15) sum2 of
      Nothing   -> Nothing
      Just sum3 -> case nums 100 sum3 of 
        Nothing   -> Nothing
        Just sum4 -> Just sum4   

{- 
  Można prościej

  Wykorzystajmy monady! Najprościej to chyba pokazać.
  Monada to nic innego jak abstrakcja nad tym lasem ifów ponad.
-}

{- 
  Definicja monady - i wszystko jasne!
-}

{- 
class Applicative m => Monad (m :: * -> *) where
  (>>=) :: m a -> (a -> m b) -> m b
  (>>) :: m a -> m b -> m b
  return :: a -> m a
  fail :: String -> m a
        instance Monad (Either e) -- Defined in ‘Data.Either’
        instance Monad [] -- Defined in ‘GHC.Base’
        instance Monad Maybe -- Defined in ‘GHC.Base’
        instance Monad IO -- Defined in ‘GHC.Base’
        instance Monad ((->) r) -- Defined in ‘GHC.Base’
        instance Monoid a => Monad ((,) a) -- Defined in ‘GHC.Base’
-}

{- 
  funcja (>>=) przyjmuje coś co jest instancją monady 
  i funckję która działa na typie jakim monada jest parametryzowana a zwracającą cos co jest jej instancją
  monada po prostu zajmuje się "kontekstem" działania i w zależności od niego coś robi 
-}

-- Jeśli w trakcie obliczeń przekroczymy 10 to dostaniemy Nothing
-- Jeśli nie to Just wartość
exNums2 = return 0 >>= nums 1 >>= nums 4 >>= nums (-15)
exNums3 = return 0 >>= nums 1 >>= nums 14 >>= nums (-15) -- ostateczna suma jest mniejsza niż 10!

printExNums = do
  putStr "return 0 >>= nums 1 >>= nums 4 >>= nums (-15) = "
  print exNums2
  putStr "return 0 >>= nums 1 >>= nums 14 >>= nums (-15) = "
  print exNums3

-- Jeśli w trakcie obliczeń przekroczymy 10 to dostaniemy Left "More than 10!"
-- Jeśli nie to Right wartość
exEitherN1 = return 0 >>= eitherN 1 >>= eitherN 4 >>= eitherN (-15)
exEitherN2 = return 0 >>= eitherN 12 >>= eitherN (-10) -- ostateczna suma jest mniejsza niż 10

printExEither = do
  putStr "return 0 >>= eitherN 1 >>= eitherN 4 >>= eitherN (-15) = "
  print exEitherN1
  putStr "return 0 >>= eitherN 12 >>= eitherN (-10) = "
  print exEitherN2


{- 
instance Monad Maybe where  
    return x = Just x  
    Nothing >>= f = Nothing  
    Just x >>= f  = f x  
    fail _ = Nothing 

instance Monad (Either a) where
  return r = Right r
  (Left l) >>= _ = Left l
  (Right r) >>= f = f r
-}

{- 
  Można powiedzieć, że jeśli potraktować typy generyczne jako kontekt obliczeń, to monada to jest to co zajmuje się tym kontekstem w zdefiniowany sposób.
  Nie jest jedynym sposobem na zajęcie się nim - bo można jeszcze funktorami aplikatywnymi i zwykłymi

  Lista jest typem generycznym! I jest instancją monady

  Kontekstem jest w tym przypadku, że element może być jeden a może być ich wiele albo wcale
-}

exList = do
  putStr "[1..10] >>= (\\x -> [x+1]) = " 
  print $ [1..10] >>= (\x -> [x+1]) 
  putStr "[]      >>= (\\x -> [x+1]) = " 
  print $ [] >>= (\x -> [x+1]) 

