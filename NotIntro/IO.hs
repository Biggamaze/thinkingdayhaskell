import Data.Char 

{-
  Haskell bardzo mocno oddziela funkcje czyste (pure - takie dla których wyjście dla danego wejścia jest zawsze takie samo) od nieczystych.
  Funkcje nieczyste (impure) to np.: wypisanie czegoś na konsolę, odczyt czegoś z bazy danych, komunikacja przez HTTP.
  Takie rzeczy mają efekty uboczne oraz mogą dawać różne wyniki - bo zawołanie 2 razy czegoś z bazy może dać inne dane, albo może za drugim razem polecieć wyjątek.

  Istnieje specjalny typ : IO a, ktory jest znacznikiem takich akcji.
  Akcje posiadające efekty uboczne jak np.: print mają typ IO () to znaczy, że nie zwracają wartości (void)
  Akcje zwracające wartość np.: getLine mają typ np.: IO (String), co znaczy, że po ich wykonaniu otrzymamy String
-}

ex1 :: IO ()
ex1 = print "Goodbye World"

ex2 :: IO (String)
ex2 = getLine 

{-
  Jeśli chcemy "odpakować te dane" musimy "wykonać" tą akcję
  Istnieją dwa sposoby syntaktyczne (z czego jeden jest ładniej napisanym drugim)
-}

ex3 :: IO ()
ex3 = getLine >>= print 

ex4 :: IO ()
ex4 = do
  val <- getLine
  print val -- ma typ ostatniego wyrażenia

{-
  Wykorzystujemy do tego operacje na monadach. Składnia z "do" działa także dla innych typów ktore są instancją monady.

  IO samo w sobie jest typem - nie ma tu wielkiej magii w użyciu. Jedyne co to to, że nie możemy wykonywać na nim pattern matchingu.
  To też nie jest magia, bo po prostu nie mamy do nich dostępu, efekt taki można osiągnąć nawet w naszym kodzie.

  Jeszcze kilka przykładów
-}

ex5 :: IO ()
ex5 = do
  putStrLn "Give me some input: "
  line <- getLine
  putStrLn $ ("Uppercase: "++) . map toUpper $ line