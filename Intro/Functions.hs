{- 
  Kilka podstawowych funkcji
-}


-- funkcja stała, można rzec
one :: Int
one = 1

-- funkcja przyjmująca parametr
increment :: Int -> Int
increment x = x + 1

increment' :: Int -> Int 
increment' x = x + one

-- funkcja z dwoma parametrami
add :: Int -> Int -> Int
add x y = x + y

{-  
  funkcja przyjmująca inną funkcję (funkcja wyższego rzędu) 
  C# ~> typ parametru : Func<int, int>
-}
applyFunction :: (Int -> Int) -> Int -> Int
applyFunction f input = f input

applyFunctionTwoTimes :: (Int -> Int) -> Int -> Int
applyFunctionTwoTimes f input = f (f input)


exParam = do
  putStr "one = "
  print one
  putStr "increment 1 = "
  print (increment 1)
  putStr "increment' 1 = "
  print (increment' 1)
  putStr ("add 1 1 = ")
  print (add 1 1)
  putStr ("1 `add` 1 = ")
  print (1 `add` 1) -- infiksowa aplikacja funkcji
  putStr "applyFunction increment 1 = "
  print (applyFunction increment 1)
  putStr "applyFunctionTwoTimes increment 1 = "
  print (applyFunctionTwoTimes increment 1)

-- //////////////////////////////////////////////////////////////////////////

{- 
  Przechodzimy schodek wyżej
-}

-- rekurencja -- działa jak w innych językach, kwestia zapisu
recurse :: Int -> Int
recurse x = if x > 0 then x + recurse (x - 1) else 0

-- zapis tego samego z pomocą tzw. strażników (guards)
recurseG x
  | x > 0 = x + recurseG (x-1)
  | otherwise = 0 -- otherwise jest zdefiniowane jako : otherwise = True

-- no i silnia 
factorial x 
  | x < 2 = 1
  | otherwise = x * factorial (x-1)


-- //////////////////////////////////////////////////////////////////////////


{- 
  Wyrażenia lokalne w zapisie funckji
-}

{- 
  wyrażenie let:
  definijemy wyrażenia wykorzystywane później po słowie "in"
-}

letMe x = let inc = increment x
          in add x inc
          
letMe2 x = let inc = increment x
               inc2 = increment (2*x)
           in add inc inc2

{- 
  wyrażenie where:
  w zapisie na końcu funkcji
  przypomnijcie sobie zapis wzorów na matmie : jakiś wzór .. GDZIE stała1 = ..., f(x) = ... itd.
-}

whereTo x = add x inc
  where inc = increment x

whereTo2 x = add inc inc2
  where inc = increment x
        inc2 = increment (x*2)

-- To co jesy zdefiniowane w let albo where nie musi być wartością - może być funkcją!

letMe3 x = let f = add 
         in f 1 x

whereTo3 x = f x
  where f v = add 1 v

-- //////////////////////////////////////////////////////////////////////////


{- 
  Currying - częśiowa aplikacja funkcji - nazwa pochodzi od amerykańskiego matematyka Haskella Curry'ego
  Tak.
  Dobrze się domyślacie.

  Do rzeczy - wymyślmy sobie funkcję:
-}

mult :: Int -> Int -> Int -> Int
mult x y z = x * y * z

mult' :: Int -> Int -> Int 
mult' y z = mult 3 y z

{- 
  można też zapisać w stylu "point free" czyli bez nazw zmiennych. Kompilator sam dedukuje, że powinny tam być 
  warto pamiętać, że wtedy podajemy pierwszy z lewej argument. zawsze
-}
mult'' :: Int -> Int
mult'' = mult' 3 

mult''' :: Int
mult''' = mult'' 3

-- //////////////////////////////////////////////////////////////////////////


{- 
  Działania na kolekcjach: map, filter
  Jest ich więcej, ale te poruszę
-}

-- map :: (a -> b) -> [a] -> [b]
{- 
  argumenty :
  1. funkcja przekształacająca typ a na b
  2. lista z elementami typu a

  zwraca : lista z elementami typu b
-}

mapToString = map show [1..20] -- show to takie ToString()

{- 
  zwracam uwagę na currying (*2) :: Int -> Int 
  element listy wskakuje jako drugi argument
-}
mapTimes2 = map (*2) [1..20] 

exMap = do
  putStr "map (*2) [1..20] = "
  print mapTimes2
  putStr "map show [1..20] = "
  print mapToString

-- filter :: (a -> Bool) -> [a] -> [a]

filterPositive = filter (>0) [-1,2,-12,12,-100,111,21212]
filterPositive' = filter (\x -> x > 0) [-1,2,-12,12,-100,111,21212] -- lambdy działają jak wszędzie
filtersEqual = filterPositive == filterPositive'

exFilter = do
  putStr "filter (>0) [-1,2,-12,12,-100,111,21212] = "
  print filterPositive
  putStr "filter (\\x -> x > 0) [-1,2,-12,12,-100,111,21212] = "
  print filterPositive'
  putStr "Equal ? "
  print filtersEqual

-- jak mogą te funckje być zaimplementowane?

myMap :: (a -> b) -> [a] -> [b]
myMap _ [] = []
myMap f (x:xs) = f x:(myMap f xs)

myFilter :: (a -> Bool) -> [a] -> [a]
myFilter _ [] = []
myFilter f (x:xs) = if f x then x:(myFilter f xs) else myFilter f xs

-- //////////////////////////////////////////////////////////////////////////

{- 
  Składanie funkcji
  pamiętacie f(x) = h(g(x)) ??

  funkcje składa się za pomocą operatora .
  który sam jest funkcją!
  (.) :: (b -> c) -> (a -> b) -> a -> c

  zauważcie, że obie funckje muszą przyjmować dokładnie jeden argument
-}

incrementMax x y = (increment . max x) y -- increment(max(x,_)) ("_" to miejsce na twoją zmienną)

-- wybiera maksymalną sumę z sublist powstałych w wyniku pomnożenia wejściowych sublist przez 2
doStuff list = composedFunc list
  where composedFunc = maximum . map action -- maximum(map(action, _))
        action subl = (sum . map (*2)) subl -- sum(map((*2),_))

-- funkcje można składać wielokrotnie
manyComp list = replicate (foldl (+) 0 . map (length . show) . filter (>0) . map (*2) $ list) "RAMMSTEIN"

exComp = do
  putStr "incrementMax 10 20 = "
  print $ incrementMax 10 20
  putStr "doStuff [[1,2],[1,3,3],[1..70],[(-90)..100]] = "
  print $ doStuff [[1,2],[1,3,3],[1..70],[(-90)..100]]
  putStr "manyComp [1..10] ++ [(-12)..10] = "
  print $ manyComp $ [1..10] ++ [(-12)..10]

{- 
  Przykład leniwości Haskella
-}

lazyEr = take 3 [1,2,undefined]

lazy = take 2 [1,2,undefined]

lazy2 = let u = undefined
        in 1 + 1

lazy3 = 1 + 1
  where u = undefined
  

{- 
  Na koniec jeden ze sztandarowych przykładów ekspresywności języków funkcyjnych
  Co to za algorytm?
-}

func [] = []
func (x:xs) = func [l | l <- xs, l < x] ++ [x] ++ func [r | r <- xs, r >= x]