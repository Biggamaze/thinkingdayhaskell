{- 
  Idea jest taka - mamy typ i jego konstruktory danych.
  Typ jest czymś abstrakcyjnym - jak na przykład typ płatności albo kolor.
  Konstruktory z kolei czymś konkretniejszym - np.: gotówka albo czerwony

  Należenie do typu jest więc czymś w rodzaju należenia do pewnej kategorii.
-}
data PaymentType = Cash | CreditCard

{- funkcja w której wykorzystujemy pattern matching na naszym typie -}
pay :: PaymentType -> String
pay Cash = "By cash"
pay CreditCard = "By card"

ex1 = do
  let a = Cash
  let b = CreditCard
  putStrLn (pay a)
  putStrLn (pay b)


-- /////////////////////////////////////////////////////////////////////////////


{- 
  Generyki
  Możemy parametryzować typy innymi typami np.: odpowiednikiem MyClass<int> jest MyType Int
  Lista jest też generykiem takim samym jak MyType, 
  ale z uwagi na wielką popularność list ma specjalną składnię
-}

data Tree a = Empty
            | Node a (Tree a) (Tree a)
            deriving (Show) -- "auto instancje" -> więcej o tym przy typeclassach

{- Jakkolwiek konstruktor danych może mieć inną nazwę, 
  to już określenie typu jaki przyjmuje musi być konstruktorem typu -}

data Spaghetti = Spaghetti | Bolognese 

{- 
  Po lewej stronie "=" : konstruktor typu wraz z parametrami, coś jak np. Tree<T>
  Po prawej stronie "=" : kontruktory danych, funkcjnalnie jest to coś jak "new" ale to bardzo dalekie porównanie

  Można to zrozumieć jakoś : drzewo 
  albo jest puste (Empty, jest liściem) 
  albo jest węzłem z zawartością typu a i dwoma dziećmi

  Przykład jak się tym bawić:
-}

ex2 = do
  let t1 = Empty 
  let t2 = Empty
  let t3 = Node 1 t1 t2
  let t4 = Node 2 Empty t3

  putStr "Tree = "
  print t4

-- Inne przykłady :

{- 
  Kolor jest pewną abstrakcją, a konkretne kolory już czymś bardziej konkretnym
  Czyli Red, Green, Blue są Color. 
  Z drugiej strony Color jest albo Red albo Green albo Blue
-}
data Color = Red | Green | Blue

{- 
  Dwa parametry - limitu na nie nie ma 
  TwoTypes<T1,T2>  
-}
data TwoTypes a b = One a 
                  | Two b 
                  | Three a b 
                  | Four a a b b
                  deriving (Show)

-- Record syntax

data Record a = Record { name :: a
                       , surname :: String
                       }
              | EmptyRecord
              deriving (Show)

exConstruct = do
  print $ ((One 1)::TwoTypes Int Int)
  print $ ((Two 1)::TwoTypes Int Int)
  print $ ((Three 1 2)::TwoTypes Int Int)
  print $ ((Four 1 2 "a" "b")::TwoTypes Int String)
  print "---"
  -- konstruować możemy to jak chcemy
  print $ (Record {name = "Anna", surname = "Boleyn"})
  print $ (Record "Anna" "Boleyn")


-- /////////////////////////////////////////////////////////////////////////////


-- A TERAZ BARDZO WAŻNE DWA TYPY WBUDOWANE

{- 
  data Maybe a = Just a | Nothing
  data Either a b = Left a | Right b
-}

-- pamiętacie wybuchające "head" ? obiecałem pokazać lepszy sposób
headMaybe :: [a] -> Maybe a
headMaybe [] = Nothing
headMaybe (x:xs) = Just x

exMaybe = do
  let lst1 = []::[Int]
  putStr "headMaybe [] = "
  print (headMaybe lst1)

  let lst2 = [1,2,3]
  putStr "headMaybe [1,2,3] = "
  print (headMaybe lst2)


-- a teraz z Either a b
headEither :: [a] -> Either String a
headEither [] = Left "Empty list"
headEither (x:xs) = Right x

exEither = do
  let lst1 = []::[Int]
  putStr "headEither [] = "
  print (headEither lst1)

  let lst2 = [1,2,3]
  putStr "headEither [1,2,3] = "
  print (headEither lst2)