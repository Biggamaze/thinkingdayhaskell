{- 
  Listy czyli chleb powszedni programowanie funkcyjnego
  Nie jest to znana nam z Javy czy C# tablica z możliwością dodawania elementów o zamorytzowanym koszcie
  To jest prawdziwa linked lista
-}

empty = []
l1 = [1,2,3] -- Prosta definicja
l2 = [1..20] -- Od do
l3 = [1..] -- Haskell jest leniwy - nieskończona lista
l4 = [1,3..] -- Można robić "wzorce"

l5 = [(1,"a"), (2, "b")] -- lista może być bardziej skomplikowana, tu jej elementami są tuple

l6 = [x | x <- l2] -- tzw. list comprehension obecne w wielu językach np. Python
l61 = [x | x <- l2, x < 4] -- z dodanym warunkiem

l7 = [y | (y,b) <- l5] -- Pattern matching tupla
l71 = [b | (y,b) <- l5]
l72 = [ fst y | y <- l5] -- podobny efekt możemy osiągnąć aplikując funkcję na wynik
l73 = [ snd y | y <- l5]

main = do
  putStrLnE ("l1 = [1,2,3] = " ++ (show l1))
  putStrLnE ("l2 = [1..5] = " ++ (show l2))
  putStrLnE ("l5 = [(1,\"a\"), (2, \"b\")] = " ++ (show l5))
  putStrLnE ("l6 = [x | x <- l2] = " ++ (show l6))
  putStrLnE ("l61 = [x | x <- l2, x < 4] = " ++ (show l61))
  putStrLnE ("l7 = [y | (y,b) <- l5] = " ++ (show l7))
  putStrLnE ("l71 = [b | (y,b) <- l5] = " ++ (show l71))
  putStrLnE ("l72 = [ fst y | y <- l5] = " ++ (show l72))
  putStrLnE ("l73 = [ snd y | y <- l5] = " ++ (show l73))
  
{- 
  Niektórych list nie wypisuję bo GHCI nie rozumie słowa "niemożliwe" w przypadku wypisywania nieskończonych list.
-}

{- 
  Może jeszcze krótko o tuplach
-}

t2 :: (Int,String)
t2 = (1, "a")

t3 :: (Int, String, Bool)
t3 = (2, "b", True)
  
-- impl (ładne wypisywanie)
putStrLnE str = putStrLn str >> putStrLn "" 
  