{-  
  Pattern matching pozwala nam wychwycić przypadek kiedy dany "obiekt" został skonstruowany za pomocą konkretnego konstruktora

  (:) jest konstruktorem list 
  [1,2] można zapisać jako 1:2:[] (pierwszy zapis to tzw. syntactic sugar) - działanie jest prawostronnie łączne, więc:
  [1,2,3] = 1:2:3:[] = 1:(2:(3:[])) 

  de facto jest to dokładnie kolejnych elementów do pustej listy
  czyli (x:xs) złapie nam (pierwszy_element:reszta)

  stąd konwencja nazewnictwa x jako liczba pojedyncza xs jako "wiele x-ów"
-}

-- weźmy sobie pierwszy element listy - głowę
getHead1 :: [a] -> a
getHead1 (x:xs) = x

-- "a" tutaj oznacza typ polimorficzny
-- lista jest typem generycznym

exHead = do
  putStr "getHead1 [1,2] = "
  print (getHead1 [1,2])
  putStr "getHead1 [1] = "
  print (getHead1 [1])
  putStr "getHead1 [] = "
  print (getHead1 []::[Int])

{-
  Skąd błąd?  
  a jak wygląda konstruktor pustej listy?
  O TAK : []
  więc nie zostaje złapany błąd - funkcja nie wie co robić
-}

--naprawmy to:
getHead2 :: a -> [a] -> a
getHead2 def [] = def
getHead2 _ (x:xs) = x -- konwencja : "_" oznacza, że nie obchodzi nas wartość argumentu

-- za chwilę przedstawię lepszy sposób na ogarnięcie tego - bez wartości domyślnej

exHead2 = do
  putStr "getHead2 0 [1,2] = "
  print (getHead2 0 [1,2])
  putStr "getHead2 0 [1] = "
  print (getHead2 0 [1])
  putStr "getHead2 0 [] = "
  print (getHead2 0 ([]::[Int]))

{- 
  taki zapis jak wyżej jest tylko syntactic sugar na switch na wzorcach - podobny do tego z C# bodajże 7.0
-}

getHead3 :: a -> [a] -> a
getHead3 def lst = case lst of 
  [] -> def
  (x:xs) -> x

-- ciekawostka : funkcja "head" ze standardowej biblioteki jest zaimplementowana podobnie jak getHead1