{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}

{- 
  Typeclassa to taki interfejs, akurat ta analogia się trzyma :)

  Opisuje ona jakie funkcje powinno implementować coś co jest jej instancją.
  Może zawierać domyślne implementacje

  Zaimplementujmy więc coś na kształt wartości truthy/falsy z Pythona/JS
-}

class Truthy a where
  toBool :: a -> Bool

{- 
  Mamy klasę typów z deklaracją jednej funkcji 

  Stwórzymy "instancję" (już drugi "false friend" w tym pliku :)
-}

instance Truthy String where
  toBool val 
    | length val == 0 = False
    | otherwise = True

instance Truthy Bool where 
  toBool val = val

-- PRZYKŁAD WYKORZYSTANIA

exTypeClass = do
  putStr "toBool \"anna\" = "
  print $ toBool "anna"
  putStr "toBool \"\" = "
  print $ toBool ""
  putStr "toBool True = "
  print $ toBool True
  putStr "toBool False = "
  print $ toBool False

{- 
  A co jeśli nie chcę pisać instancji dla każdego typu z numerycznych? Int, Double, Float etc.
-}

newtype Numy a = Numy a -- alias dla typu, kompilator tego wymaga :(
instance (Num a, Eq a) => Truthy (Numy a) where
  toBool (Numy val) 
    | val == 0 = False
    | otherwise = True

exTypeClass2 = do
  putStr "toBool 1::Int = "
  print $ toBool (Numy (1:: Int))
  putStr "toBool 2.1::Double = "
  print $ toBool (Numy (2.1::Double))
  putStr "toBool 0::Int = "
  print $ toBool (Numy (0:: Int))
  putStr "toBool 0::Double = "
  print $ toBool (Numy (0::Double))
  
{-
  Definicja listy i typeclassy jakich instancją jest:

  data [] a = [] | a : [a]        -- Defined in ‘GHC.Types’
  instance Eq a => Eq [a] -- Defined in ‘GHC.Classes’
  instance Monad [] -- Defined in ‘GHC.Base’
  instance Functor [] -- Defined in ‘GHC.Base’
  instance Ord a => Ord [a] -- Defined in ‘GHC.Classes’
  instance Read a => Read [a] -- Defined in ‘GHC.Read’
  instance Show a => Show [a] -- Defined in ‘GHC.Show’
  instance Applicative [] -- Defined in ‘GHC.Base’
  instance Foldable [] -- Defined in ‘Data.Foldable’
  instance Traversable [] -- Defined in ‘Data.Traversable’
  instance Monoid [a] -- Defined in ‘GHC.Base’
-}

{- 
  Prawdopodobnie nie jest to jeszcze czas na monady, ale mamy tylko 1 godzinę ;)
-}