# Linki do przydatnych stron:
## Tutoriale
  1. [Learn You Haskell For Great Good](http://learnyouahaskell.com/chapters) - według mnie bardzo dobrze tłumaczy sam język. Ciekawa forma.
  2. [RealWorldHaskell](http://book.realworldhaskell.org/read/) - nieco bardziej surowy, ale tłumaczy także takie zagadnienia jak webówka, bazy danych, równoległość. 
  3. [Pluralsight](https://www.pluralsight.com/courses/haskell-fundamentals-part1) - nie oglądałem, ale jak ktoś woli w taki sposób to warto spróbować.
 
----------

## Inne materiały

- [Hoogle](https://www.haskell.org/hoogle/) - Haskell Google. Dokumentacja języka i bibliotek. Pozwala wyszukiwać po typach funkcji (np.: Int -> Int)
- [Stack](https://docs.haskellstack.org/en/stable/README/) - najświeższy build system dla Haskella. Oparty na Cabalu, ma swoje wady ale generalnie działa. Odpowiednik NuGeta + MSBuild w .NET. 
- [Haskell Platform](https://www.haskell.org/platform/) - idealne narzędzie do zabawy z Haskellem. Instaluje kompilator GHC wraz z najpotrzebniejszymi bibliotekami i GHCI. W wersji na różne systemy.
- [Haskero](https://gitlab.com/vannnns/haskero) - rozszerzenie do VS Code zmieniające edytor w takie trochę IDE. Oparte na Stacku oraz Intero, więc wymaga zainstalowania ich najpierw (instrukcja w linku). Alternatywą może być Haskelly, ale z niego nie korzystałem.
- [IntelliJ-Haskell](https://plugins.jetbrains.com/plugin/8258-intellij-haskell) - rozszerzenie do IntelliJ. Także oparte na Stacku oraz Intero, ale chyba oferuje więcej niż rozszerzenie do VS Code. Dość skomplikowana konfiguracja niestety.
- [Gentle Introduction to Haskell](https://www.haskell.org/tutorial/index.html) - tak naprawdę to tutorial, ale żeby nikt nie miał mi za złe, że to wpisałem w tutoriale, to dodaję tutuaj. Bardzo surowo opisane różnie językowe konstrukty. Dodaję raczej dlatego, że czasem warto poczytać o rzeczach na różne sposoby, jeśli by ktoś czegoś nie mógł zrozumieć.
